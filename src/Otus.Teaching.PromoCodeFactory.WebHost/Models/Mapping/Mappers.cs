using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Mapping;

public class Mappers
{
    public static Employee EmployeeFactory(EmployeeJson employeeJson)
    {
        var roles = RolesFactory(employeeJson);

        var employee = new Employee
        {
            Id = Guid.NewGuid(),
            FirstName = employeeJson.FirstName,
            LastName = employeeJson.LastName,
            Email = employeeJson.Email,
            Roles = roles,
            AppliedPromocodesCount = employeeJson.AppliedPromocodesCount
        };

        return employee;
    }

    public static List<Role> RolesFactory(EmployeeJson employeeJson)
    {
        var roles = new List<Role>();
        foreach (var roleJson in employeeJson.Roles)
        {
            roles.Add(new Role
            {
                Id = Guid.NewGuid(),
                Name = roleJson.Name,
                Description = roleJson.Description
            });
        }

        return roles;
    }
    
    public static Employee UpdatedEmployeeFactory(Employee oldEmployee, EmployeeJson newEmployeeMapping)
    {
        var updatedEmployee = new Employee
        {
            Id = oldEmployee.Id,

            FirstName = newEmployeeMapping.FirstName != string.Empty ? newEmployeeMapping.FirstName : oldEmployee.FirstName,

            LastName = newEmployeeMapping.LastName != string.Empty ? newEmployeeMapping.LastName : oldEmployee.LastName,

            Email = newEmployeeMapping.Email != string.Empty ? newEmployeeMapping.Email : oldEmployee.Email,

            AppliedPromocodesCount = newEmployeeMapping.AppliedPromocodesCount != 0
                ? newEmployeeMapping.AppliedPromocodesCount
                : oldEmployee.AppliedPromocodesCount,

            Roles = newEmployeeMapping.Roles.Count != 0 ? Mappers.RolesFactory(newEmployeeMapping) : oldEmployee.Roles
        };
        return updatedEmployee;
    }
}