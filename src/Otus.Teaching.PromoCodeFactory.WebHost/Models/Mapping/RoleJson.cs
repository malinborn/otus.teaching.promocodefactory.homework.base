namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Mapping;

public class RoleJson
{
    public string Name { get; set; }

    public string Description { get; set; }
}