using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Mapping;

public class EmployeeJson
{
    public string FirstName { get; set; }
    
    public string LastName { get; set; }

    public string Email { get; set; }

    public List<RoleJson> Roles { get; set; }

    public int AppliedPromocodesCount { get; set; }
}