﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task Add(T entity)
        {
            if (entity != null)
            {
                return Task.FromResult(Data.Append(entity));
            }

            throw new NullReferenceException("given entity is empty");
        }

        public Task<T> DeleteByIdAsync(Guid id)
        {
            var deletedEmployee = Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
            if (deletedEmployee is null)
            {
                throw new NullReferenceException("there is no such employee");
            }
            Data = Data.Where(x => x.Id != id);

            return deletedEmployee;
        }

        public Task Update(T entity)
        {
            DeleteByIdAsync(entity.Id);
            return Task.FromResult(Add(entity));
        }
    }
}